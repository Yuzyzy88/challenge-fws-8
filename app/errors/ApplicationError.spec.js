const ApplicationError = require("./ApplicationError")

describe("ApplicationError", () => {
    it("details should return empty", () => {
        const applicationError = new ApplicationError();
        expect(applicationError.details).toStrictEqual({})
    })
    it("toJSON should return empty", () => {
        const applicationError = new ApplicationError("test message");
        applicationError.name = "testname"
        const json = applicationError.toJSON();
        expect(json.error.name).toStrictEqual("testname")
        expect(json.error.message).toStrictEqual("test message")
        expect(json.error.details).toStrictEqual({})
    })

})