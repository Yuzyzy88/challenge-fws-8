const express = require("express");
const jwt = require("jsonwebtoken");
const dayjs = require("dayjs");
const bcrypt = require("bcryptjs");
const {
    User,
    Role,
    Car,
    UserCar,
} = require("../models");

const {
    ApplicationController,
    AuthenticationController,
    CarController,
} = require("../controllers");
const { JsonWebTokenError } = require("jsonwebtoken");

const carModel = Car;
const roleModel = Role;
const userModel = User;
const userCarModel = UserCar;

const applicationController = new ApplicationController();
const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel, });
const carController = new CarController({ carModel, userCarModel, dayjs });

const { EmailNotRegisteredError, EmailAlreadyTakenError } = require('../errors');
const { user } = require("pg/lib/defaults");

describe("AuthenticationController", () => {
    beforeEach(async () => {
        const names = [
            "Johnny",
            "Fikri",
            "Brian",
            "Ranggawarsita",
            "Jayabaya",
        ]

        const password = "123456";
        const encryptedPassword = bcrypt.hashSync(password, 10);
        const timestamp = new Date();

        const role = await Role.findOne({
            where: {
                name: "CUSTOMER",
            }
        })

        const users = names.map((name) => ({
            name,
            email: `${name.toLowerCase()}@binar.co.id`,
            encryptedPassword,
            roleId: role.id,
            createdAt: timestamp,
            updatedAt: timestamp,
        }))
       
        await Promise.all(users.map(user => User.create(user)))
    })
    afterEach(async () => {
        await User.destroy({ where: {} });
    })
    it("handleGetUser", async () => {

        const mockResponse = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        }

        const user = await User.findOne({
            where: {
                name: "Johnny",
            }
        });

        const role = await Role.findOne({
            where: {
                name: "CUSTOMER"
            }
        })
        const mockRequest = {
            user: {
                id: user.id,
            }
        }

        if (!user) {
            const err = new Error("Not found")
            await authenticationController.handleGetUser(mockRequest, mockResponse)
            expect(mockResponse.status).toHaveBeenCalledWith(404)
            expect(mockResponse.json).toHaveBeenCalledWith(err)
        }

        if (!role) {
            const err = new Error("Not found")
            await authenticationController.handleGetUser(mockRequest, mockResponse)
            expect(mockResponse.status).toHaveBeenCalledWith(404)
            expect(mockResponse.json).toHaveBeenCalledWith(err)
        }

        await authenticationController.handleGetUser(mockRequest, mockResponse)
        expect(mockResponse.status).toHaveBeenCalledWith(200)
        expect(mockResponse.json).toHaveBeenCalledWith(user)

    })
    it('handleRegister should register with unused email', async () => {
        const name = "Ayu"
        const email = "ayu@gmail.com"
        const password = "customer123"

        const role = await Role.findOne({
            where: {
                name: 'CUSTOMER'
            }
        })


        const mockRequest = {
            body: {
                email, name, password
            }
        }

        const mockResponse = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        }
        const mockNext = jest.fn()

        await authenticationController.handleRegister(mockRequest, mockResponse, mockNext)
        expect(mockResponse.status).toHaveBeenCalledWith(201)
    })

    it("handleRegister should not register with already used email", async () => {

        const name = "Ayu"
        const email = "ayu@gmail.com"
        const password = "customer123"

        const role = await Role.findOne({
            where: {
                name: 'CUSTOMER'
            }
        })

        const user = await User.create({
            name: name,
            email: email,
            encryptedPassword: authenticationController.encryptPassword(password),
            roleId: role.id
        });
        await user.save()

        const existingUser = await User.findOne({
            where: {
                email: user.email
            }
        });

        const accessToken = authenticationController.createTokenFromUser(user, role);

        const mockRequest = {
            body: {
                email: existingUser.email
            }
        }

        const mockResponse = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        }
        const mockNext = jest.fn()

        const err = new EmailAlreadyTakenError(user.email)
        await authenticationController.handleRegister(mockRequest, mockResponse, mockNext)
        expect(mockResponse.status).toHaveBeenCalledWith(422)
        expect(mockResponse.json).toHaveBeenCalledWith(err)
    })

    it("handleLogin user not register", async () => {

        const email = "ayu@gmail.com"
        const password = "customer123"

        const mockRequest = {
            body: {
                email, password
            }
        }
        const mockResponse = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        }
        const mockNext = jest.fn()

        await authenticationController.handleLogin(mockRequest, mockResponse, mockNext)
        expect(mockResponse.status).toHaveBeenCalledWith(404)
    });
    it("authorize correct", async () => {
        const role = await Role.findOne({
            where: {
                name: 'CUSTOMER'
            }
        });
        const email = "ayu@gmail.com"
        const password = "customer123"
        const user = await User.create({
            name: "Ayu",
            email: email,
            encryptedPassword: authenticationController.encryptPassword(password),
            roleId: role.id
        });
        await user.save();
        token = await authenticationController.createTokenFromUser(user, role);

        const mockRequest = {
            body: {
                email, password
            },
            headers: {
                authorization: `Bearer ${token}`
            }
        }
        const mockResponse = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        }
        const mockNext = jest.fn()
        await authenticationController.authorize('CUSTOMER')(mockRequest, mockResponse, mockNext)
        expect(mockNext).toHaveBeenCalled()

    });
    it("authorize incorrect", async () => {
        const role = await Role.findOne({
            where: {
                name: 'CUSTOMER'
            }
        });
        const email = "ayu@gmail.com"
        const password = "customer123"
        const user = await User.create({
            name: "Ayu",
            email: email,
            encryptedPassword: authenticationController.encryptPassword(password),
            roleId: role.id
        });
        await user.save();
        const mockRequest = {
            body: {
                email, password
            },
            headers: {
                authorization: `Bearer wrongtoken`
            }
        }
        const mockResponse = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        }
        const mockNext = jest.fn()
        await authenticationController.authorize('CUSTOMER')(mockRequest, mockResponse, mockNext)
        expect(mockResponse.status).toHaveBeenCalledWith(401)
    });
    it("handleLogin pass correct", async () => {
        const role = await Role.findOne({
            where: {
                name: 'CUSTOMER'
            }
        });
        const email = "ayu@gmail.com"
        const password = "customer123"
        const user = await User.create({
            name: "Ayu",
            email: email,
            encryptedPassword: authenticationController.encryptPassword(password),
            roleId: role.id
        });
        await user.save()

        const mockRequest = {
            body: {
                email, password
            }
        }
        const mockResponse = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        }
        const mockNext = jest.fn()
        await authenticationController.handleLogin(mockRequest, mockResponse, mockNext)
        expect(mockResponse.status).toHaveBeenCalledWith(201)

    });
    it("handleLogin pass not correct", async () => {
        const role = await Role.findOne({
            where: {
                name: 'CUSTOMER'
            }
        });
        const email = "ayu@gmail.com"
        const password = "customer123"
        const user = await User.create({
            name: "Ayu",
            email: email,
            encryptedPassword: authenticationController.encryptPassword(password),
            roleId: role.id
        });
        await user.save()

        const mockRequest = {
            body: {
                email, password: "wrong password"
            }
        }
        const mockResponse = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
        }
        const mockNext = jest.fn()
        await authenticationController.handleLogin(mockRequest, mockResponse, mockNext)
        expect(mockResponse.status).toHaveBeenCalledWith(401)

    });

    it("#createTokenFromUser", async () => {
        const johnny = await User.findOne({
            where: {
                name: "Johnny"
            }
        });
        const role = await Role.findOne({
            where: {
                name: "CUSTOMER",
            }
        })
        const token = authenticationController.createTokenFromUser(johnny, role)
        expect(token).not.toBe("")
    })
    it("#decodeToken", async () => {
        const johnny = await User.findOne({
            where: {
                name: "Johnny"
            }
        });
        const role = await Role.findOne({
            where: {
                name: "CUSTOMER",
            }
        })
        const token = authenticationController.createTokenFromUser(johnny, role);
        const user = authenticationController.decodeToken(token);
        expect(user.email).toBe("johnny@binar.co.id")
        expect(user.name).toBe("Johnny")
    })
    it("#encryptPassword", async () => {
        const pass = authenticationController.encryptPassword("password")
        expect(pass).not.toBe("")
    })
    it("#verifyPassword", async () => {
        const johnny = await User.findOne({
            where: {
                name: "Johnny"
            }
        });
        const role = await Role.findOne({
            where: {
                name: "CUSTOMER",
            }
        })
        const verification = authenticationController.verifyPassword("password", "$2y$10$dGs4vGZuvY22l5EUfFBRI.j/iukBcgklHtNWXugaVhw71hUnouHSW")
        expect(verification).toBe(true)
    })
})