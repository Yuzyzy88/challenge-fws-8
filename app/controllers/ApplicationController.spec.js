const ApplicationController = require("./ApplicationController")
const { NotFoundError } = require("../errors");

describe("ApplicationController", () => {
    describe("#handleGetRoot", () => {
        it("should call res.status (200) and res.json", async () => {
            const status = "OK"
            const message = "BCR API is up and running!"

            const mockRequest = {}
            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            };

            const applicationController = new ApplicationController()

            await applicationController.handleGetRoot(mockRequest, mockResponse)

            expect(mockResponse.status).toHaveBeenCalledWith(200)
            expect(mockResponse.json).toHaveBeenCalledWith({ status, message })
        });
    });

    describe("#handleNotFound", () => {
        it("should call res.status (404) and res.json with error instance", async () => {
            const mockRequest = {
                method: jest.fn().mockReturnThis(),
                url: jest.fn().mockReturnThis(),
            }
            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            }

            const err = new NotFoundError(mockRequest.method, mockRequest.url);
            const applicationController = new ApplicationController()

            await applicationController.handleNotFound(mockRequest, mockResponse)

            expect(mockResponse.status).toHaveBeenCalledWith(404)
            expect(mockResponse.json).toHaveBeenCalledWith({
                error: {
                    name: err.name,
                    message: err.message,
                    details: err.details,
                },
            });
        });
    });

    describe("#handleError", () => {
        it("should call res.status (500) and res.json with error instance", async () => {
            const err = new Error("Not found!")

            const mockRequest = {}
            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            }

            const applicationController = new ApplicationController()

            await applicationController.handleError(err, mockRequest, mockResponse)

            expect(mockResponse.status).toHaveBeenCalledWith(500)
            expect(mockResponse.json).toHaveBeenCalledWith({
                error: {
                    name: err.name,
                    message: err.message,
                    details: err.details || null,
                }
            })
        });
    });

    describe("#getOffsetFromRequest", () => {
        it("should get offset from request", async () => {
            const mockRequest = {
                query: {
                    page: 1,
                    pageSize: 10
                }
            }

            const applicationController = new ApplicationController()

            const offset =  applicationController.getOffsetFromRequest(mockRequest)

            expect(offset).toBe(0)
        });
    });
    describe("#buildPaginationObject", () => {
        it("should get build pagination object from request and count", async () => {
            const mockRequest = {
                query: {
                    page: 1,
                    pageSize: 10
                }
            }

            const applicationController = new ApplicationController()

            const res =  applicationController.buildPaginationObject(mockRequest, 0)

            expect(res.page).toBe(1)
            expect(res.pageCount).toBe(0)
            expect(res.pageSize).toBe(10)
            expect(res.count).toBe(0)
        });
    });
});