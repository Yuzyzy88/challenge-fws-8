const jwt = require("jsonwebtoken");
const dayjs = require("dayjs");
const bcrypt = require("bcryptjs");
const {
    User,
    Role,
    Car,
    UserCar,
} = require("../models");

const {
    ApplicationController,
    AuthenticationController,
    CarController,
} = require("../controllers");
const { JsonWebTokenError } = require("jsonwebtoken");
const { query } = require("express");

const carModel = Car;
const roleModel = Role;
const userModel = User;
const userCarModel = UserCar;

const applicationController = new ApplicationController();
const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel, });
const carController = new CarController({ carModel, userCarModel, dayjs });


describe("CarController", () => {
    beforeEach(async () => {
        const names = [
            "Johnny",
            "Fikri",
            "Brian",
            "Ranggawarsita",
            "Jayabaya",
        ]

        const password = "123456";
        const encryptedPassword = bcrypt.hashSync(password, 10);
        const timestamp = new Date();

        const role = await Role.findOne({
            where: {
                name: "CUSTOMER",
            }
        })

        const users = names.map((name) => ({
            name,
            email: `${name.toLowerCase()}@binar.co.id`,
            encryptedPassword,
            roleId: role.id,
            createdAt: timestamp,
            updatedAt: timestamp,
        }))
        await Promise.all(users.map(user => User.create(user)))

        const carnames = [
            "Mazda RX4",
            "Mazda RX4 Wag",
            "Datsun 710",
            "Hornet 4 Drive",
            "Hornet Sportabout",
            "Valiant",
            "Duster 360",
            "Merc 240D",
            "Merc 230",
            "Merc 280",
            "Merc 280C",
            "Merc 450SE",
            "Merc 450SL",
            "Merc 450SLC",
            "Cadillac Fleetwood",
            "Lincoln Continental",
            "Chrysler Imperial",
            "Fiat 128",
            "Honda Civic",
            "Toyota Corolla",
            "Toyota Corona",
            "Dodge Challenger",
            "AMC Javelin",
            "Camaro Z28",
            "Pontiac Firebird",
            "Fiat X1-9",
            "Porsche 914-2",
            "Lotus Europa",
            "Ford Pantera L",
            "Ferrari Dino",
            "Maserati Bora",
            "Volvo 142E",
        ]

        const sizes = ["SMALL", "MEDIUM", "LARGE"];
        const cars = [];

        sizes.forEach((size) => {
            cars.push(
                ...carnames.map((name, i) => {
                    const accumulator = i.toLocaleString('en-US', {
                        minimumIntegerDigits: 2,
                        useGrouping: false
                    });

                    const timestamp = new Date();

                    return ({
                        name,
                        price: 300000,
                        size,
                        image: `https://source.unsplash.com/5${accumulator}x5${accumulator}`,
                        isCurrentlyRented: false,
                        createdAt: timestamp,
                        updatedAt: timestamp,
                    })
                })
            )
        })

        await Promise.all(cars.map(car => Car.create(car)))
    })
    afterEach(async () => {
        await UserCar.destroy({ where: {} })
        await Car.destroy({ where: {} })
    })

    describe("#handleListCars", () => {
        it("should call res.status (200) and res.json", async () => {

            const mockRequest = {
                query: {
                    page: 1,
                    pageSize: 10,
                }
            }

            const query = await carController.getListQueryFromRequest(mockRequest)

            const cars = await Car.findAll({
                mockRequest
            })

            const carCount = await Car.count({
                where: query.where,
                include: query.include
            })

            const pageCount = Math.ceil(carCount / mockRequest.query.pageSize)

            const pagination = {
                page: mockRequest.query.page,
                pageCount,
                pageSize: mockRequest.query.pageSize,
                count: carCount
            }

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            }

            await carController.handleListCars(mockRequest, mockResponse)
            expect(mockResponse.status).toHaveBeenCalledWith(200)
        });
    });

    describe("#handleGetCar", () => {
        it("should call res.status (200) and res.json", async () => {
            const car = await Car.findOne({
                where: {
                    name: 'Mazda RX4',
                }
            });

            const mockRequest = {
                params: {
                    id: car.id
                }
            }

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            }
            await carController.handleGetCar(mockRequest, mockResponse)
            expect(mockResponse.status).toHaveBeenCalledWith(200)
            expect(mockResponse.json).toHaveBeenCalledWith(car)

        });
    });

    describe("#handleCreateCar", () => {
        it("should call res.status (201) and res.json", async () => {


            const mockRequest = {
                body: {
                    name: "",
                    price: 100,
                    size: "SMALL",
                    image: "https://source.unsplash.com/500x500",
                }
            }

            const car = await Car.create({
                body: { mockRequest }
            })

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            }

            const err = new Error("Failed create new car");
            await carController.handleCreateCar(mockRequest, mockResponse)
            expect(mockResponse.status).toHaveBeenCalledWith(201)

        });
        it("should call res.status (422) and res.json", async () => {


            const mockRequest = {
                body: {
                    name: "",
                    price: "",
                    size: "",
                    image: "ht",
                }
            }

            const car = await Car.create({
                body: { mockRequest }
            })

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            }

            const err = new Error("Failed create new car");
            await carController.handleCreateCar(mockRequest, mockResponse)
            expect(mockResponse.status).toHaveBeenCalledWith(422)
        });
    })

    describe("#handleRentCar", () => {
        it("should call res.status (201) and res.json", async () => {

            const car = await Car.findOne({ where: {} })
            const user = await User.findOne({ where: {} })
            const mockRequest = {
                body: {
                    rentStartedAt: "2022-01-01",
                    rentEndedAt: "2022-01-30",
                },
                params: {
                    id: car.id
                },
                user,
            }


            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            }
            const mockNext = jest.fn();

            await carController.handleRentCar(mockRequest, mockResponse, mockNext)
            expect(mockResponse.status).toHaveBeenCalledWith(201)
        });
        xit("should call res.status (422) and res.json", async () => {

            const car = await Car.findOne({ where: {} })
            const user = await User.findOne({ where: {} })
            const activeRent = await UserCar.create({
                userId: user.id,
                carId: car.id,
                rentStartedAt: new Date("2020-01-03 00:00:00"),
                rentEndedAt: new Date("2020-01-27 00:00:00"),
                createdAt: new Date(),
                updatedAt: new Date()
            });
            await activeRent.save();
            const mockRequest = {
                body: {
                    rentStartedAt: new Date("2020-01-01 00:00:00"),
                    rentEndedAt: new Date("2020-01-30 00:00:00"),
                },
                params: {
                    id: car.id
                },
                user
            }


            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            }
            const mockNext = jest.fn();

            await carController.handleRentCar(mockRequest, mockResponse, mockNext)
            expect(mockResponse.status).toHaveBeenCalledWith(422)
        });
    })

    describe("#handleUpdateCar", () => {
        
        it("should call res.status (200) ", async () => {

            const car = await Car.findOne({ where: {} })
            const mockRequest = {
                body: {
                    price: car.price,
                    size: car.size,
                    image: car.image,
                    name: 'Toyota'
                },
                params: {
                    id: car.id
                }
            }


            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            }

            await carController.handleUpdateCar(mockRequest, mockResponse)
            expect(mockResponse.status).toHaveBeenCalledWith(200)
        });

        it("should call res.status (422) and res.json", async () => {

            const car = await Car.findOne({ where: {} })
            const mockRequest = {}


            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            }

            await carController.handleUpdateCar(mockRequest, mockResponse)
            expect(mockResponse.status).toHaveBeenCalledWith(422)
        });
    })


    describe("#handleDeleteCar", () => {
        it("should call res.status (204) and res.json", async () => {

            const mockRequest = {
                params: {
                    id: 1
                }
            }

            const car = await Car.findOne({
                where: {
                    name: 'Mazda RX4'
                }
            });

            const mockCar = {}
            mockCar.destroy = jest.fn().mockReturnValue(car)

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                end: jest.fn().mockReturnThis(),
            }

            const carController = new CarController({ carModel: mockCar });
            await carController.handleDeleteCar(mockRequest, mockResponse)
            expect(mockResponse.status).toHaveBeenCalledWith(204)
            expect(mockResponse.end).toHaveBeenCalled()

        });
    });

    it("getListQueryFromRequest", async () => {
        const car = await Car.findOne({ where: {} })
        const mockRequest = {
            query: {
                size: 10,
                availableAt: 1,
            },
            params: {
                id: car.id
            }
        }
        const res = carController.getListQueryFromRequest(mockRequest);
        expect(res).not.toBeNull();
    })

    describe("#getCarFromRequest", () => {
        it("should get car from request", async () => {
            const mockRequest = {
                params: {
                    id: 1
                }
            }

            const car = await Car.findOne({
                where: {
                    name: 'Mazda RX4'
                }
            });

            await carController.getCarFromRequest(mockRequest)
            expect(car.name).toBe("Mazda RX4")
        });
    });



});